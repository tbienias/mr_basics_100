# MR Basics 100 - Getting started with Unity #
## Content ##
This example shows hwo to setup Unity projects for Microsoft HoloLens.
It renders a basic cube. This example is meant to be run on the HoloLens Device or on the HoloLens Emulator.

![](preview_0.png)

## Requirements ##
Unity 2017.4.13f1

Visual Studio 2017

## Credits ##
All credits go to Microsoft who provided this tutorial on https://docs.microsoft.com/en-us/windows/mixed-reality/holograms-100.